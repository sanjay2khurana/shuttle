import React, { Component, PropTypes } from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter as Router,
  Route,
  browserHistory,
  Switch,
  Redirect
} from "react-router-dom";

import Cookie from "react-cookies";

import App from "./public/src/app";

import LoginLayout from "./public/src/containers/loginLayout";
import SearchPage from "./public/src/pages/Search/";

import LoginPage from "./public/src/pages/User/login";

import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
/* Middleware */
import thunk from "redux-thunk";
let order = [thunk];
const middleware = applyMiddleware(...order);

import allReducers from "./public/src/reducers/index";

let store = createStore(allReducers, middleware);
const isAuthenticatedArr = {
  SearchPage: <SearchPage />
};
const isAuthenticated = props => {
  if (!GLOBAL.isloggedIn || Cookie.load("uuid") == undefined) {
    localStorage.clear("user");
    return <Redirect to="/app/login" />;
  }
  return isAuthenticatedArr[props.componentName];
};

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <div>
        <Switch>
          <Route path="/app/login" component={LoginPage} />
          <Route exact path="/" component={SearchPage} />
        </Switch>
      </div>
    </Router>
  </Provider>,
  document.getElementById("root")
);
