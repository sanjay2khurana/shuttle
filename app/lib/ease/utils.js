'use strict';
var acceptableStatuses = [200, 201, 404, 401, 400];

var uuid = require('node-uuid');
let _ = require('lodash');

var toArray = function (obj) {
    var isArray = Object.prototype.toString.call(obj) === '[object Array]';
    return isArray ? obj : [obj];
};

var fixContentType = function (contentType) {
    return contentType ? ((contentType === 'object' || contentType === 'string') ? contentType : 'object' ): 'object';
};



var queryStringFromObj = function (obj) {
    obj = obj || {};
    var str = '';
    _.forEach(obj, function (value, key) {
        str += key + '=' + value + '&';
    });
    if (str !== '') {
        str = str.slice(0, -1);
    }
    return str;
};

/**
 * Getting User's IP
 * @param headers
 * @returns {*}
 */



var generateUrl = function (requestedServer, endPoint) {
    if(!requestedServer || !endPoint){
        throw new Error(' Invalid generateURL Call, RequestedServer ' + JSON.stringify(requestedServer) + ' End Point -> ' + JSON.stringify(endPoint));
    }
    let serverNode = requestedServer;
    let protocolStr = serverNode.protocol || 'http';
    let serverNameStr = serverNode.server || 'localhost';
    let portStr = serverNode.port ? ':' + serverNode.port : '';
    return (protocolStr + '://' + serverNameStr + portStr + endPoint);
};


module.exports = {
    toArray: toArray,
    fixContentType: fixContentType,
    queryStringFromObj: queryStringFromObj,
   
    isBadStatus: function (statusCode) {
        return !_.find(acceptableStatuses, function (status) {
            return status === statusCode;
        });
    },

    replacePlaceHolders: function (map, option) {
        _.each(map, function (value, key) {
            option.endPoint = option.endPoint.replace('{' + key + '}', value);
        });
    },    
    
    generateQueryString: function (content) {
        var queryString = '';
        switch (typeof content) {
            case 'string' :
                queryString = content;
                break;
            case 'object' :
                queryString = queryStringFromObj(content);
                break;
            default :
        }
        return queryString.length ? '?' + queryString : '';
    },

    generateUrl: generateUrl,
};
