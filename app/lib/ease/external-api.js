"use strict";

let path = require("path");
let utils = require("./utils");

let Promise = require("bluebird");
let _ = require("lodash");

let request = Promise.promisify(
  require("request").defaults({ pool: { maxSockets: Infinity } })
);
let externalRouteConfigs = require("../../config/external-api-services/external-routes");
class ExternalApiCommServices {
  constructor(serviceConfigs) {
    this.store = {
      requestConfigs: [] //Array of request to be sent
    };
    this.apiResponse = []; //Array of response Data from API
    this.serviceConfigs = serviceConfigs;
    this.req = {};
    this.stats = {
      initTime: process.hrtime()
    };
    this.easeConfig = global.conf.config;
    this.apiTimingFlag = this.easeConfig.trackApiTimings;
    this.externalServers = this.easeConfig.serverConfigs;
  }
  fetchData() {
    this._buildConfigData();
    return Promise.map(this.store.requestConfigs, option => {
      option.headers = {
        "Content-Type": "application/json"
      };

      return this._requestWithRetry(option)
        .then(response => {
          this._pushData(option, response.body, response.headers);
          console.log(this.apiResponse);
          return this.apiResponse;
        })
        .catch(e => {
          console.log("Error", e);
          if (option.fromCache) {
          } else {
            //  this._errorHandler(e, option);
          }
        });
    }).then(response => {
      delete this.store.requestConfigs;
      this.apiResponse = this.getSanitizedData(this.apiResponse);
      return this.apiResponse;
    });
  }
  /**
     * This function is used to send the request to the API
     * @param options Request options
     * @returns {Promise.<T>|*}
     */
  _requestWithRetry(options) {
    var currentStat = { start: process.hrtime() };
    console.log(options);
    //Enabling gzip compression
    options.gzip = true;
    return request(options)
      .then(response => {
        let apiResponse = response;
        let statusCode = apiResponse.body.status.httpStatus;
        // console.log("------------->", response[1]);
        // let data = typeof response[1] === 'object' ? response[1] : JSON.parse(response[1]);
        var data;
        try {
          data =
            typeof apiResponse === "object"
              ? apiResponse
              : JSON.parse(apiResponse);
        } catch (e) {
          console.log("Inside Catch");
          data = {
            status: {
              success: false,
              errors: [
                {
                  message: apiResponse,
                  code: 404
                }
              ]
            },
            success: false,
            str: apiResponse
          };
          response = JSON.stringify(data);
        }
        // if (utils.isBadStatus(statusCode) && _.isEmpty(response[1])) {
        //   //this._throwErrorWithStatusCode(statusCode, options.url, response[1]);
        // }
        return data;
      })
      .catch(e => {
        options.retry = options.retry || 1;
        if (
          !options.noretry &&
          !options.trivial &&
          options.retry < (this.easeConfig.apiRetries || 3)
        ) {
          options.retry++;
          options.timeout =
            (options.timeout || 0) +
            (Math.floor(
              Math.random() * (this.easeConfig.apiTimeOutUpperEntropy - 1)
            ) +
              this.easeConfig.apiTimeOutLowerEntropy) *
              1000;
          return this._requestWithRetry(options);
        } else {
          console.log("throw error");
          throw e;
        }
      });
  }
  _buildConfigData() {
    let configs = utils.toArray(this.serviceConfigs);
    configs.forEach(config => {
      if (!config) {
        throw new CustomError(
          new Error("Empty Service config Array " + config)
        );
      }
      if (
        typeof config === "object" &&
        typeof Object.keys(config)[0] === "string"
      ) {
        let serviceConfigKey = Object.keys(config)[0];
        let serviceConfigParams = config[serviceConfigKey];
        let routeConfig = externalRouteConfigs[serviceConfigKey];
        let externalRouteConfig = routeConfig;

        // if (!externalRouteConfig) {
        //     throw new CustomError(new Error('No ExternalRoute Config found for given key ' + serviceConfigKey));
        // }
        let externalRouteConfigOptions = externalRouteConfig.options;
        externalRouteConfigOptions.serviceIdentifier =
          serviceConfigParams.tag || serviceConfigKey;
        serviceConfigParams.content = serviceConfigParams.content
          ? serviceConfigParams.content
          : {};
        this._buildExternalRouteConfig(
          serviceConfigParams,
          externalRouteConfigOptions
        );
        //this._buildHeader(serviceConfigParams, externalRouteConfigOptions);
        this._buildRequestUrl(serviceConfigParams, externalRouteConfigOptions);
        this._pushConfig(externalRouteConfigOptions, false);
      } else {
        throw new CustomError(new Error("Service config passed is not valid!"));
      }
    });
  }
  /**
     *
     * @private
     */
  _buildPayLoad(serviceConfigParams, externalRouteConfigOptions) {
    if (externalRouteConfigOptions.method === "get") {
      externalRouteConfigOptions.endPoint += utils.generateQueryString(
        serviceConfigParams.content
      );
    } else {
      let targetType = externalRouteConfigOptions.json ? "body" : "form";
      externalRouteConfigOptions[targetType] = serviceConfigParams.content;
    }
    return externalRouteConfigOptions;
  }
  _buildRequestUrl(serviceConfigParams, externalRouteConfigOptions) {
    utils.replacePlaceHolders(
      serviceConfigParams.urlConfig,
      externalRouteConfigOptions
    );
    this._buildPayLoad(serviceConfigParams, externalRouteConfigOptions);
    let requestedServer = this.externalServers[
      externalRouteConfigOptions.server
    ];
    externalRouteConfigOptions.url =
      serviceConfigParams.url ||
      utils.generateUrl(requestedServer, externalRouteConfigOptions.endPoint);
    return externalRouteConfigOptions;
  }

  _buildExternalRouteConfig(serviceConfigParams, externalRouteConfigOptions) {
    externalRouteConfigOptions.method = (externalRouteConfigOptions.method ||
      "get")
      .toLowerCase();
    externalRouteConfigOptions.contentType = utils.fixContentType(
      serviceConfigParams.contentType
    );
    externalRouteConfigOptions.timeOut =
      externalRouteConfigOptions.timeout ||
      (externalRouteConfigOptions.timeout
        ? this.easeConfig.timeouts[externalRouteConfigOptions.timeout]
        : this.easeConfig.timeout);
    externalRouteConfigOptions.trivial =
      serviceConfigParams.trivial || externalRouteConfigOptions.trivial;
    return externalRouteConfigOptions;
  }

  /**
     * This function is used to push data in Collection member
     * @param option Request options
     * @param data Retrieved response from API
     */
  _pushData(option, data, headers) {
    this.apiResponse.push({
      option: option,
      data: data,
      headers: headers
    });
  }

  /**
     * This function is used to sanitize and combine all the responses
     * @param collectedData Response Collected
     * @param dump
     * @returns Single Response Object
     */
  getSanitizedData(collectedData, dump) {
    var finalData = {};
    _.each(collectedData, function(d) {
      var data = d.data;
      var urlConfig = d.option;
      try {
        if (typeof data !== "object") {
          if (_.isEmpty(data)) {
            var error = new Error("No Data Fetched");
            error.res = data;
            error.rethrow = !d.option.ignoreResponse;
            throw error;
          }
          data = JSON.parse(data);
        }
      } catch (e) {
        if (e.rethrow && !urlConfig.trivial) {
          log.error(
            "External Services : Rethrowed : EMPTY RESPONSE from External API for " +
              urlConfig.url,
            null,
            { tid: urlConfig.headers["X-Jabong-Tid"] }
          );
          throw e;
        } else {
          data = {
            error: "Not a valid JSON or not found any data",
            responseJson: data
          };
        }
      }
      if (dump) {
        data.urlConfig = urlConfig;
      }
      finalData[urlConfig.serviceIdentifier] = data;
      finalData["headers"] = d.headers;
    });

    return finalData;
  }

  /**
     *
     * @param externalRouteConfigOptions
     * @param doCache
     * @private
     */

  _pushConfig(externalRouteConfigOptions, doCache) {
    if (doCache) {
      externalRouteConfigOptions.hash = Crypto.createHash("sha256")
        .update(externalRouteConfigOptions.url)
        .digest("hex");
      this.store.cacheRequestConfigs.push(externalRouteConfigOptions);
    } else {
      this.store.requestConfigs.push(externalRouteConfigOptions);
    }
  }
}

module.exports = exports = {
  /**
     * This function is used by the external modules to send request to initialize External API
     * @param params Configuration
     * @param req Request Object
     */
  request: function(params) {
    if (!params) {
      throw new Error("No Params Passed");
    }
    return new ExternalApiCommServices(params);
  }
};
