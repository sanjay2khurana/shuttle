module.exports = {
  testConfig: true,
  serverConfigs: {
    test_srv: {
      protocol: "https",
      server: "dev.jabong.com"
    },
    ums_srv: {
      protocol: "http",
      server: "services.technogramsolutions.com"
    }
  }
};
