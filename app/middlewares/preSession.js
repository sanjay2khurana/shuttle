const session = require("fastify-session");
const jwt = require("jsonwebtoken");

const Session = (request, reply, next) => {
  next();
};

module.exports = Session;
