/**
 * Middleware List to be executed before each Request
 */
let BaseMiddleware = require("./base-middleware");

module.exports = {
  order: {
    preHandler: [""],
    onSend: [""]
  },
  initialize: function(app) {
    let middleware;
    this.order.preHandler.forEach(function(item) {
      middleware = require("./" + item);
      let baseMiddlwareObj = new BaseMiddleware();
      baseMiddlwareObj.init(app, "preHandler", middleware);
    });
    this.order.onSend.forEach(function(item) {
      middleware = require("./" + item);
      let baseMiddlwareObj = new BaseMiddleware();
      baseMiddlwareObj.init(app, "onSend", middleware);
    });
  }
};
