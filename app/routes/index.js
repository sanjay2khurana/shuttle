const fastify = require("fastify")();
const homeRoute = require("./homepage");

const routes = [homeRoute];

module.exports = routes;
