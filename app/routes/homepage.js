const homepage = {
  method: "GET",
  url: "/",
  handler: function(req, reply) {
    const { isloggedIn = false, userInfo = {} } = req;
    reply.view("index", {
      isAuthenticated: isloggedIn,
      userInfo: userInfo
    });
  }
};

module.exports = homepage;
