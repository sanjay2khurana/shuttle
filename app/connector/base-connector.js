"use strict";
let ExternalApiService = require("./../lib/ease/external-api.js");

/**
 * Base Model Class
 */
class BaseConnector {
  /**
     * Constructor to set the response object
     */
  constructor() {
    this.modelSchema = {};
  }

  /**
     * This function is used to send request to external api for getting the response
     * @param req Request
     * @param config Request Configuration
     * @returns {Promise|*}
     */
  request(config) {
    //console.log('Inside Base Model', config);
    return ExternalApiService.request(config).fetchData();
  }
}
module.exports = exports = BaseConnector;
