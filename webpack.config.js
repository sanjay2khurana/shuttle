const path = require("path");

module.exports = {
  entry: { app: "./index.js" },
  output: {
    path: path.resolve(__dirname) + "public/js",
    filename: "bundle.js"
  },
  resolve: {
    extensions: [".js", ".jsx"]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: path.resolve(__dirname) + "/node_modules",
        loader: "babel-loader",
        query: {
          presets: ["es2017", "react", "stage-3", "babel-preset-es2017"]
        }
      },
      {
        test: /\.jsx$/,
        loader: "babel-loader",
        include: path.resolve(__dirname) + "/public/themes/",
        query: {
          presets: ["es2017", "react", "stage-3", "babel-preset-es2017"]
        }
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      },
      {
        test: /.*\.(gif|png|jpe?g|svg)$/i,
        use: [
          {
            loader: "url-loader",
            options: {
              name: "./../img/[name].[ext]"
            }
          }
        ]
      }
    ]
  }
};
