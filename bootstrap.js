const routes = require("./app/routes");

const _ = require("lodash");

const customRoutes = fastify => {
  _.forEach(routes, function(route) {
    fastify.route(route);
  });
  return fastify;
};

const bootstrap = {
  customRoutes
};

module.exports = bootstrap;
