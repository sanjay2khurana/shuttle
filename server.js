let fastify = require("fastify")();
const bootstrap = require("./bootstrap");
const config = require("./app/config/env/development");
//const RedisSession = require("./app/config/redisSession")(fastify);
const middlewares = require("./app/middlewares");

const fastifyCookie = require("fastify-cookie");
fastify.register(fastifyCookie);

global.conf = { config };

bootstrap.customRoutes(fastify);

middlewares.initialize(fastify);

const path = require("path");
const serveStatic = require("serve-static");

// Single path
fastify.use("/js", serveStatic(path.join(__dirname, "/public/js")));

// Wildcard path
fastify.use("/js/*", serveStatic(path.join(__dirname, "/public/js")));

// Wildcard path
fastify.use("/css", serveStatic(path.join(__dirname, "/public/css")));

// Multiple paths
fastify.use([".css", ".js"], serveStatic(path.join(__dirname, "/public")));

fastify.register(require("point-of-view"), {
  engine: {
    ejs: require("ejs")
  },
  includeViewExtension: true
});

fastify.listen(3030, function(err) {
  console.log(".......................||...$`...........");
  console.log("......................||||...$`...........");
  console.log(".....................||||||...$`...........");
  console.log("....................|||||||||..................");
  console.log("..................|||||||||||||$`...........");
  console.log(".............../o|||||||||||||||o+-...............");
  console.log("...........-oh|||||||||||||||||||||||$`...........");
  console.log("........./h||||||||||||||||||||||||||||$..........");
  console.log(".......:h||||||||||||||||||||||||||||||||o`.......");
  console.log("......+d||||||||||||||||||||||||||||||||||h`......");
  console.log(".....:m||||yh+//mh||||||||||||||d$:/$hy|||hh......");
  console.log(".....h|||$/-...-dM||||||||||||||M+....:oy||m-.....");
  console.log("....`m|y:.......yM||||||||||||||N:......-+|ho.....");
  console.log("....:dh$........+Mh||||||||||||Nd-.......-dyh.....");
  console.log("....+dhy........-Nd||||||||||||M$........:dhm.....");
  console.log("....$hhy-........ym|||||||||||hM:......../dhm`....");
  console.log("....hhdo/........:Nm|||||||||ddh........./dhm-....");
  console.log("....dhdo:..........o$|||||||o.........`/-dhm:....");
  console.log("....dhdo:..........o$$|||||$$o.........`/-dhm:....");
  console.log("....dhdo:..........o$$$|||$$o.........`/-dhm:....");
  console.log("....dhdo:...........o$$$|$$o.........`/-dhm:....");
  console.log("================ FLIGHTS TOOK OFF ==============");
  console.log(err);
});
