import React from "react";

class ClassBox extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (e, className) => {
    this.props.handleChange(className);
  };

  render() {
    const isChecked =
      this.props.isChecked == this.props.className ? true : false;

    return (
      <div class="mb-20">
        <b>{this.props.className}</b>{" "}
        <span class="pull-right">
          <div
            class="round"
            onClick={e => this.handleChange(e, this.props.className)}
          >
            <input type="radio" checked={isChecked} name="passengerClass" />
            <label for="checkbox" />
          </div>
        </span>
      </div>
    );
  }
}

export default ClassBox;
