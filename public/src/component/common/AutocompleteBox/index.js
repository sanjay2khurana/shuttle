import React from "react";
import Autosuggest from "react-autosuggest";
import style from "./style.css";
import axios from "axios";
import AirportList from "./../../../config/airportList.json";

function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

function getSuggestions(value) {
  if (value) {
    return axios
      .get(
        "http://services.technogramsolutions.com/fms/v1/airports/search/" +
          value
      )
      .then(response => {
        return response.data.airportList;
      });
  } else {
    return AirportList.airportList;
  }
}

function getSuggestionValue(suggestion) {
  return `${suggestion.city}(${suggestion.code})`;
}

function shouldRenderSuggestions(value) {
  return value.trim().length >= 0;
}
function renderSuggestion(suggestion) {
  return (
    <span class="at-topcity">
      <span>
        <i class="fa fa-map-marker" aria-hidden="true" /> {suggestion.city} ({suggestion.code})
      </span>
    </span>
  );
}

class Autocomplete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      suggestions: this.props.defaultList,
      defaultList: this.props.defaultList,
      context: this.props.context
    };
    if (this.handleContext == "undefined") {
      this.handleContext = this.handleContext.bind(this);
    }
  }

  onChange = (event, { newValue, method }) => {
    this.setState({
      value: newValue
    });
    this.props.handleContext(this.state.context, newValue);
  };

  onSuggestionsFetchRequested = async ({ value }) => {
    let updatedSuggestions = await getSuggestions(value);
    this.setState({
      suggestions: updatedSuggestions
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: this.props.defaultText,
      value: this.props.defaultValue ? this.props.defaultValue : "",
      onChange: this.onChange
    };

    return (
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        inputProps={inputProps}
        shouldRenderSuggestions={shouldRenderSuggestions}
      />
    );
  }
}

export default Autocomplete;
