import _ from "lodash";

const SearchUrl = tripInfo => {
  let url = "";
  const { journeyType } = tripInfo;
  const { oneway, returnway, multicity } = journeyType;
  let urlObj = {};
  if (oneway || returnway) {
    const { fixedTrip } = tripInfo;

    const { origin, destination, depDate } = fixedTrip;
    const from = origin.match(/\((.*)\)/)[1];
    const to = destination.match(/\((.*)\)/)[1];
    let dep = depDate.format("DD-MM-YYYY");
    urlObj = { from, to, dep };
    if (returnway) {
      const { retDate } = fixedTrip;
      const ret = retDate.format("DD-MM-YYYY");
      urlObj = { ...urlObj, ret };
    }
  } else if (multicity) {
    let { flexiTrip, fixedTrip, segmentCount } = tripInfo;
    const {
      origin: origin_0,
      destination: destination_0,
      depDate: depDate_0
    } = fixedTrip;
    flexiTrip = { ...flexiTrip, origin_0, destination_0, depDate_0 };
    _.each(flexiTrip, (obj, key) => {
      const [source, count] = key.split("_");
      if (source.indexOf("Date") > -1) {
        obj = obj.format("DD-MM-YYYY");
      }
      if (source.indexOf("origin") > -1 || source.indexOf("destination") > -1) {
        obj = obj.match(/\((.*)\)/)[1];
      }
      if (count && count <= segmentCount) {
        const keyObj = { [key]: obj };
        urlObj = { ...urlObj, ...keyObj };
      }
    });
  }
  // adding pax and class Info to URL
  const { ADT, CHD } = tripInfo.paxInfo;
  const { pClass } = tripInfo;

  urlObj = { ...urlObj, ADT, CHD, pClass };

  let urlString = Object.keys(urlObj)
    .map(key => key + "=" + urlObj[key])
    .join("&");
  console.log(urlString);
};

export default SearchUrl;
