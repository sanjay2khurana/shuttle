import React from "react";
import CalendarBox from "./../CalendarBox";
import AirportList from "./../../../config/airportList.json";
import AutocompleteBox from "./../AutocompleteBox";
import moment from "moment";
const MulticityBox = props => {
  let count = [...Array(props.count)];

  let multiCityDiv = count.map((i, key) => {
    let calendarMinDate =
      key == 0
        ? props.fixedTripInfo["depDate"]
        : props.flexiTripInfo[`depDate_${key}`];

    return (
      <div>
        <div class="pt-20" />
        <div class="form-group at-iconform label-value at-exchange">
          <i class="fa fa-plane" />
          <AutocompleteBox
            defaultList={AirportList.airportList}
            defaultText="Where From ?"
            handleContext={props.selectAirport}
            context={`origin_${key + 1}`}
            defaultValue={props.flexiTripInfo[`origin_${key + 1}`]}
          />
        </div>
        <div class="form-group at-iconform label-value">
          <i class="fa fa-plane" />
          <AutocompleteBox
            defaultList={AirportList.airportList}
            defaultText="Where To ?"
            handleContext={props.selectAirport}
            context={`destination_${key + 1}`}
            defaultValue={props.flexiTripInfo[`destination_${key + 1}`]}
          />
        </div>
        <div class="form-group at-iconform label-value">
          <i class="fa fa-calendar" />
          <CalendarBox
            placeholder="Departure Date"
            handleContext={props.selectDate}
            context={`depDate_${key + 1}`}
            minDate={moment(calendarMinDate)}
            startDate={moment(calendarMinDate)}
          />
        </div>
      </div>
    );
  });
  return multiCityDiv;
};

export default MulticityBox;
