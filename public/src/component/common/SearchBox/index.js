import React from "react";
import "./style.css";
import AutocompleteBox from "./../AutocompleteBox";
import AirportList from "./../../../config/airportList.json";
import CalendarBox from "./../CalendarBox";
import MulticityBox from "./MulticityBox";
import ClassBox from "./../ClassBox";
import NumberSelection from "./../NumberSelection";
import moment from "moment";
import SearchUrl from "./createSearchUrl";
class SearchBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      returnEnabled: false,
      oneway: true,
      return: false,
      multicity: false,
      segmentCount: 0,
      fixedTrip: {},
      flexiTrip: {},
      paxInfo: {
        ADT: 0,
        CHD: 0
      },
      pClass: "Economy",
      classBox: "close"
    };
    this.setTripInfo = this.setTripInfo.bind(this);
    this.setMulticityOrigin = this.setMulticityOrigin.bind(this);
    this.updatePaxInfo = this.updatePaxInfo.bind(this);
    this.toggleClassBox = this.toggleClassBox.bind(this);
  }

  activeReturn = () => {
    this.setState({
      returnEnabled: true,
      return: true,
      oneway: false,
      multicity: false
    });
  };

  activeOneway = () => {
    this.setState({
      returnEnabled: false,
      return: false,
      oneway: true,
      multicity: false
    });
  };

  activeMulticity = () => {
    let sCount = this.state.segmentCount ? this.state.segmentCount : 1;
    this.setState({
      returnEnabled: false,
      return: false,
      oneway: false,
      multicity: true,
      segmentCount: sCount
    });
    this.setMulticityOrigin();
  };

  addCity = () => {
    let sCount = this.state.segmentCount;
    this.setState(
      {
        segmentCount: sCount + 1
      },
      () => {
        this.setMulticityOrigin();
      }
    );
  };

  setMulticityOrigin = segmentCount => {
    let flexiTrip = this.state.flexiTrip;
    let sCount = segmentCount
      ? parseInt(segmentCount, 10) + 1
      : this.state.segmentCount;

    const origin = `origin_${sCount}`;
    let destination = "";
    if (sCount == 1) {
      destination = this.state.fixedTrip.destination;
    } else {
      destination = this.state.flexiTrip[`destination_${sCount - 1}`];
    }

    this.setState({
      flexiTrip: {
        ...flexiTrip,
        [origin]: destination
      }
    });
  };

  deleteCity = () => {
    let sCount = this.state.segmentCount;
    this.setState({
      segmentCount: sCount - 1
    });
  };

  selectAirport = (context, value) => {
    this.setState({
      [context]: value
    });
    this.setTripInfo(context, value, () => {
      const [contextType, segmentCount = "0"] = context.split("_");

      if (contextType == "destination" && segmentCount) {
        this.setMulticityOrigin(segmentCount);
      }
    });
  };

  selectDate = (context, value) => {
    console.log("date value", value);
    this.setState({
      [context]: value
    });
    this.setTripInfo(context, value);
  };

  setTripInfo = (context, value, multicitySet) => {
    let tripType = context.indexOf("_") > -1 ? "flexiTrip" : "fixedTrip";
    let tripTypeVal = this.state[tripType];
    this.setState(
      {
        [tripType]: {
          ...tripTypeVal,
          [context]: value
        }
      },
      multicitySet
    );
  };

  updatePaxInfo = (type, count) => {
    console.log("paxInfo", type, count);
    let paxInfo = this.state.paxInfo;
    this.setState({
      paxInfo: {
        ...paxInfo,
        [type]: count
      }
    });
  };

  updatePaxClass = pClass => {
    this.setState({
      pClass: pClass
    });
  };

  toggleClassBox = action => {
    console.log("toggle", action);
    this.setState({
      classBox: action
    });
  };

  createSearchUrl = () => {
    let {
      oneway,
      return: returnway,
      multicity,
      fixedTrip,
      flexiTrip,
      segmentCount,
      pClass,
      paxInfo
    } = this.state;
    let journeyType = { oneway, returnway, multicity };

    const tripInfo = {
      fixedTrip,
      flexiTrip,
      segmentCount,
      journeyType,
      pClass,
      paxInfo
    };
    const redirectUrl = SearchUrl(tripInfo);
    console.log(redirectUrl);
  };

  render() {
    const { ADT, CHD } = this.state.paxInfo;
    const paxCount = ADT + CHD;
    return (
      <div class="container">
        <div class="row">
          <h1 class="at-bookfl whitecolor mb-20">
            Book flights and explore the world with us.
          </h1>
          <ul class="nav nav-tabs">
            <li class={this.state.oneway && "active"}>
              <a
                data-toggle="tab"
                href="javascript:void(0);"
                onClick={this.activeOneway}
              >
                ONE WAY
              </a>
            </li>
            <li class={this.state.return && "active"}>
              <a
                data-toggle="tab"
                href="javascript:void(0);"
                onClick={this.activeReturn}
              >
                ROUND TRIP
              </a>
            </li>
            <li class={this.state.multicity && "active"}>
              <a
                data-toggle="tab"
                href="javascript:void(0);"
                onClick={this.activeMulticity}
              >
                MULTY CITY
              </a>
            </li>
          </ul>
          <div class="tab-content pb-20 pt-20">
            <div id="oneway" class="tab-pane fade in active">
              <form class="form-inline mb-20 form-label form-css-label">
                <div class="form-group at-iconform label-value at-exchange">
                  <i class="fa fa-plane" />
                  <AutocompleteBox
                    defaultList={AirportList.airportList}
                    defaultText="Where From ?"
                    handleContext={this.selectAirport}
                    context={"origin"}
                    defaultValue={this.state.fixedTrip.origin}
                  />
                </div>
                <div class="form-group at-iconform label-value">
                  <i class="fa fa-plane" />
                  <AutocompleteBox
                    defaultList={AirportList.airportList}
                    defaultText="Where To ?"
                    handleContext={this.selectAirport}
                    context={"destination"}
                    defaultValue={this.state.fixedTrip.destination}
                  />
                </div>
                <div class="form-group at-iconform label-value">
                  <i class="fa fa-calendar" />
                  <div class="at-returndep">
                    <CalendarBox
                      placeholder="Departure Date"
                      handleContext={this.selectDate}
                      context={"depDate"}
                      minDate={moment()}
                      startDate={moment()}
                    />
                  </div>
                  {!this.state.multicity && (
                    <div class="at-returndep at-returnin">
                      <CalendarBox
                        placeholder="Return Date"
                        class={!this.state.returnEnabled && "at-disable"}
                        handleContext={this.selectDate}
                        context={"retDate"}
                        minDate={moment()}
                        startDate={moment()}
                      />
                      <a
                        href="javascript:void(0);"
                        class="pull-right"
                        onClick={this.activeOneway}
                      >
                        <span class="at-returncut">&times;</span>
                      </a>
                    </div>
                  )}
                </div>
                <div class="form-group at-iconform label-value at-passbox">
                  <i class="fa fa-user" />
                  <input
                    class="form-control"
                    type="text"
                    autocomplete="off"
                    value={`${paxCount} Passenger | ${this.state.pClass}`}
                    onFocus={e => this.toggleClassBox("open")}
                    required
                  />
                  <label for="firstName">Passenger and Class</label>
                  <div
                    class={`at-passection ${this.state.classBox == "close" &&
                      "hide"}`}
                  >
                    <div class="at-selepass">
                      <p>SELECT PASSENGERS</p>
                      <NumberSelection
                        contextText1={"Adult"}
                        contextText2={"Age 18+"}
                        type={"ADT"}
                        handleChange={this.updatePaxInfo}
                        currentActive={this.state.paxInfo["ADT"]}
                      />
                      <div class="clearfix" />
                      <NumberSelection
                        contextText1={"Children"}
                        contextText2={"Age 2-12"}
                        type={"CHD"}
                        handleChange={this.updatePaxInfo}
                        currentActive={this.state.paxInfo["CHD"]}
                      />
                      <div class="clearfix" />
                    </div>
                    <div class="at-selright">
                      <p>SELECT CLASS</p>
                      <div class="at-economy mt-20">
                        <ClassBox
                          className={"Economy"}
                          isChecked={this.state.pClass}
                          handleChange={this.updatePaxClass}
                        />
                        <div class="clearfix" />
                        <ClassBox
                          className={"Premium Economy"}
                          isChecked={this.state.pClass}
                          handleChange={this.updatePaxClass}
                        />
                        <div class="clearfix" />
                        <ClassBox
                          className={"Business Class"}
                          isChecked={this.state.pClass}
                          handleChange={this.updatePaxClass}
                        />
                        <div class="clearfix" />
                      </div>
                    </div>
                    <div class="clearfix" />
                    <div class="at-passfooter">
                      <ul class="pull-right at-ftlist">
                        <li>
                          <a href="javascript:void(0);">CLEAR</a>
                        </li>
                        <li class="at-lsdone">
                          <a
                            onClick={e => this.toggleClassBox("close")}
                            href="javascript:void(0);"
                          >
                            DONE
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <button
                  type="button"
                  onClick={this.createSearchUrl}
                  class="btn btn-default text-uppercase whitecolor"
                >
                  search
                </button>
              </form>

              <form
                action="#"
                class={`form-inline mb-20 form-label form-css-label ${(this
                  .state.oneway ||
                  this.state.return) &&
                  "hide"}`}
              >
                {this.state.segmentCount >= 1 && (
                  <MulticityBox
                    count={this.state.segmentCount}
                    selectAirport={this.selectAirport}
                    selectDate={this.selectDate}
                    flexiTripInfo={this.state.flexiTrip}
                    fixedTripInfo={this.state.fixedTrip}
                  />
                )}
                {this.state.segmentCount > 1 && (
                  <div class="form-group">
                    <a
                      href="javascript:void(0);"
                      class="at-multyclose whitecolor"
                      onClick={this.deleteCity}
                    >
                      &times;
                    </a>
                  </div>
                )}
                {this.state.segmentCount < 5 &&
                this.state.segmentCount > 0 && (
                  <div class="form-group">
                    <button
                      type="button"
                      class="btn btn-default whitecolor at-add "
                      onClick={this.addCity}
                    >
                      Add one more
                    </button>
                  </div>
                )}
              </form>
            </div>
          </div>
          <p class="more-option">More Options : </p>
        </div>
      </div>
    );
  }
}
export default SearchBox;
