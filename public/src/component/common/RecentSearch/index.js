import React from "react";
import "./style.css";
class RecentSearch extends React.Component {
  render() {
    return (
      <div class="at-lastSearch p-20">
        <div class="container">
          <div class="row">
            <p class="at-viewlast">
              View your last search &nbsp; <span class="fa fa-angle-down" />
            </p>
          </div>
        </div>
      </div>
    );
  }
}
export default RecentSearch;
