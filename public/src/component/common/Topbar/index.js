import React from "react";
import "./style.css";
class Topbar extends React.Component {
  render() {
    return (
      <div class="topbar ptb-7">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 text-right">
              <ul class="tp-list text-uppercase">
                <li>
                  Hello JONATHAN <span>(23578)</span>{" "}
                </li>
                <li>
                  My Balance -{" "}
                  <span>
                    <i class="fa fa-inr" /> 800
                  </span>{" "}
                </li>
                <li>Dashboard</li>
                <li>Booking</li>
                <li>
                  Notifications{" "}
                  <span>
                    <sup>
                      <i class="fa fa-circle heartbit" />
                    </sup>
                  </span>{" "}
                </li>
                <li>Sales Representatives</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Topbar;
