import React from "react";
import DatePicker from "react-datepicker";
import moment from "moment";

import "react-datepicker/dist/react-datepicker.css";
import style from "./style.css";

class CalendarBox extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      context: this.props.context,
      minDate: this.props.minDate,
      startDate: this.props.startDate
    };
    this.handleChange = this.handleChange.bind(this);
    if (this.handleContext == "undefined") {
      this.handleContext = this.handleContext.bind(this);
    }
  }

  handleChange(date) {
    this.setState(
      {
        startDate: date
      },
      () => {
        this.props.handleContext(this.state.context, date);
      }
    );
  }

  componentWillReceiveProps = newProps => {
    if (newProps.minDate.isAfter(moment(this.state.startDate))) {
      this.setState({
        startDate: ""
      });
    }
    if (newProps.minDate != undefined) {
      this.setState({
        minDate: newProps.minDate
      });
    }
  };

  render() {
    return (
      <DatePicker
        fixedHeight
        selected={this.state.startDate}
        onChange={this.handleChange}
        dateFormat={"DD-MM-YYYY"}
        monthsShown={12}
        minDate={this.state.minDate}
        maxDate={moment().add(12, "months")}
        className={this.props.class}
        popperClassName="popper-class"
        placeholderText={this.props.placeholder}
      />
    );
  }
}

export default CalendarBox;
