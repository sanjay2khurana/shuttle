import React from "react";
import "./style.css";
class Footer extends React.Component {
  render() {
    return (
      <div>
        <div class="at-footermain">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <div class="at-contactleft pt-20 pb-20">
                  <h3 class="whitecolor at-conany">How can we help you?</h3>
                  <h5 class="whitecolor at-conany">Contact us anytime.</h5>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="at-contactright">
                  <div class="row">
                    <div class="col-sm-7 at-bdright">
                      <p class="text-uppercase">Sent us a message at</p>
                      <h4 class="whitecolor">support@atlastravelsonline.com</h4>
                    </div>
                    <div class="col-sm-5">
                      <p class="text-uppercase">or call us at</p>
                      <h4 class="whitecolor">1800-238-1900</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="at-footerbottom pt-40 pb-40">
          <div class="container">
            <div class="row">
              <div class="col-sm-4">
                <img src="./img/logo.png" alt="#" />
                <p class="whitecolor mt-10">
                  &copy; 2018 atlastravelsonline. All rights reserved.
                </p>
                <ul class="at-social mt-20">
                  <li>
                    <a href="#">
                      <i class="fa fa-2x fa-facebook" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-2x fa-instagram" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-2x fa-pinterest" />
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-2x fa-twitter" />
                    </a>
                  </li>
                </ul>
              </div>
              <div class="col-sm-8">
                <div class="row">
                  <div class="col-sm-3">
                    <h4 class="at-ftheading text-uppercase">More Links</h4>
                    <ul class="ft-list">
                      <li>
                        <a href="#">Terms & conditions</a>
                      </li>
                      <li>
                        <a href="#">Privacy Policy</a>
                      </li>
                      <li>
                        <a href="#">Project Protection</a>
                      </li>
                      <li>
                        <a href="#">FAQ</a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-3">
                    <h4 class="at-ftheading text-uppercase">About</h4>
                    <ul class="ft-list">
                      <li>
                        <a href="#">Meet our Team</a>
                      </li>
                      <li>
                        <a href="#">Press Kit</a>
                      </li>
                      <li>
                        <a href="#">Agents</a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-3">
                    <h4 class="at-ftheading text-uppercase">Resources</h4>
                    <ul class="ft-list">
                      <li>
                        <a href="#">Blog</a>
                      </li>
                      <li>
                        <a href="#">Advirtisement</a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-sm-3">
                    <h4 class="at-ftheading text-uppercase">Contact Us</h4>
                    <ul class="ft-list">
                      <li>
                        <a href="#">Visit Us</a>
                      </li>
                      <li>
                        <a href="#">Work With Us</a>
                      </li>
                      <li>
                        <a href="#">Email Us</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Footer;
