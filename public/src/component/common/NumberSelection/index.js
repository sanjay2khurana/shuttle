import React from "react";

class NumberSelection extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (count, type) => {
    this.props.handleChange(type, count);
  };

  render() {
    const numberCount = [...Array(9)];

    return (
      <div>
        <h4>
          {this.props.contextText1} <small>
            {this.props.contextText2}
          </small>{" "}
        </h4>
        <ul class="at-passhalf">
          {numberCount.map((x, i) => {
            const count = i + 1;
            const activeClass =
              this.props.currentActive == i + 1 ? "active" : "";
            return (
              <li
                class={activeClass}
                onClick={e => this.handleChange(count, this.props.type)}
              >
                <a href="javascript:void(0);">{i + 1}</a>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default NumberSelection;
