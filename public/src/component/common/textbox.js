import React from "react";

class Textbox extends React.Component {
  constructor(props) {
    console.log(props);
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (e, field) => {
    this.props.handleChange(e, field);
  };

  render() {
    return (
      <div class="form-group">
        <div class="input-set">
          <div class="input-part input">
            <div class="material-input">
              <input
                type={this.props.type}
                placeholder=" "
                onChange={e => this.handleChange(e, this.props.field)}
              />
              <label for={this.props.for}>{this.props.text}</label>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Textbox;
