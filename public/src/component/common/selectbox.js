import React from "react";

class SelectBox extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (e, field) => {
    this.props.handleChange(e, field);
  };

  render() {
    const selectList = this.props.options.map(val => {
      return <option value={val}>{val}</option>;
    });
    return (
      <div class="form-group">
        <div class="input-set">
          <div class="input-part input">
            <div class="material-input customSelectStyle2">
              <select
                class="effect-all"
                onChange={e => this.handleChange(e, this.props.field)}
              >
                {selectList}
              </select>
              <label for="role">Role</label>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SelectBox;
