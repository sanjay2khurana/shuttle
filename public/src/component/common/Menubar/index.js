import React from "react";
import "./style.css";
import logo from "./../../../../img/logo.png";
import Image from "./../Image";
class Menubar extends React.Component {
  render() {
    return (
      <div class="main-header">
        <div class="container">
          <div class="row">
            <div class="col-sm-3">
              <div class="navbar-header">
                <button
                  type="button"
                  class="navbar-toggle"
                  data-toggle="collapse"
                  data-target="#myNavbar"
                >
                  <span class="icon-bar" />
                  <span class="icon-bar" />
                  <span class="icon-bar" />
                </button>
                <a class="navbar-brand" href="#">
                  <Image
                    src={logo}
                    class={"img-responsive"}
                    alt={"Company Name"}
                  />
                </a>
              </div>
            </div>
            <div class="col-sm-9">
              <div class="topbar ptb-7">
                <ul class="tp-list text-uppercase">
                  <li>
                    <a href="#">
                      Hello JONATHAN <span>(23578)</span>{" "}
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      My Balance -{" "}
                      <span>
                        <i class="fa fa-inr" /> 800
                      </span>{" "}
                    </a>
                  </li>
                  <li>
                    <a href="#">Dashboard</a>
                  </li>
                  <li>
                    <a href="#">Booking</a>
                  </li>
                  <li>
                    <a href="#">
                      Notifications{" "}
                      <span>
                        <sup>
                          <i class="fa fa-circle heartbit" />
                        </sup>
                      </span>{" "}
                    </a>
                  </li>
                  <li>
                    <a href="#">Sales Representatives</a>
                  </li>
                </ul>
              </div>
              <nav class="navbar navbar-inverse">
                <div class="collapse navbar-collapse" id="myNavbar">
                  <ul class="nav navbar-nav navbar-right text-uppercase">
                    <li class="dropdown">
                      <a
                        href="#"
                        class="dropdown-toggle"
                        data-toggle="dropdown"
                      >
                        Flight <span class="fa fa-angle-down" />
                      </a>
                      <ul class="dropdown-menu">
                        <li>
                          <a href="#">Flight Search</a>
                        </li>
                        <li>
                          <a href="#">Flight Calendar</a>
                        </li>
                        <li>
                          <a href="#">Air Booking Calendar</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="#">Hotel</a>
                    </li>
                    <li>
                      <a href="#">Promotions</a>
                    </li>
                    <li>
                      <a href="#">Refer & earn</a>
                    </li>
                    <li class="dropdown">
                      <a
                        href="#"
                        class="dropdown-toggle"
                        data-toggle="dropdown"
                      >
                        Support <span class="fa fa-angle-down" />
                      </a>
                      <ul class="dropdown-menu">
                        <li>
                          <a href="#">Auto Carousel</a>
                        </li>
                        <li>
                          <a href="#">Carousel Control</a>
                        </li>
                        <li>
                          <a href="#">Left & Right Navigation</a>
                        </li>
                        <li>
                          <a href="#">Four Columns Grid</a>
                        </li>
                        <li>
                          <a href="#">Glyphicon</a>
                        </li>
                        <li>
                          <a href="#">Google Fonts</a>
                        </li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a
                        href="#"
                        class="dropdown-toggle"
                        data-toggle="dropdown"
                      >
                        My Profile <span class="fa fa-angle-down" />
                      </a>
                      <ul class="dropdown-menu at-managedrop">
                        <div class="at-mngprofile">
                          <li>
                            <a href="#">Booking Calendar</a>
                          </li>
                          <li>
                            <a href="#">Air Booking Calendar</a>
                          </li>
                          <li>
                            <a href="#">Manage Carts</a>
                          </li>
                          <li>
                            <a href="#">Manage Amendments</a>
                          </li>
                          <li>
                            <a href="#">Manage Users</a>
                          </li>
                          <li>
                            <a href="#">Manage Privileges</a>
                          </li>
                          <li>
                            <a href="#">Manage Markups</a>
                          </li>
                          <li>
                            <a href="#">Manage Requests</a>
                          </li>
                        </div>
                        <div class="at-mngprofile">
                          <li>
                            <a href="#">Booking Calendar</a>
                          </li>
                          <li>
                            <a href="#">Air Booking Calendar</a>
                          </li>
                          <li>
                            <a href="#">Manage Carts</a>
                          </li>
                          <li>
                            <a href="#">Manage Amendments</a>
                          </li>
                          <li>
                            <a href="#">Manage Users</a>
                          </li>
                          <li>
                            <a href="#">Manage Privileges</a>
                          </li>
                          <li>
                            <a href="#">Manage Markups</a>
                          </li>
                        </div>
                      </ul>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Menubar;
