import React from "react";

const Image = props => {
  return <img src={props.src} class={props.class} alt={props.alt} />;
};

export default Image;
