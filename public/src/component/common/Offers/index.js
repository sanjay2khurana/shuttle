import React from "react";
import "./style.css";
import offer1 from "./../../../../img/offer1.jpg";
import offer2 from "./../../../../img/offer2.jpg";
import Image from "./../Image";
class Offers extends React.Component {
  render() {
    return (
      <div class="at-dealsoffer pt-40 pb-40">
        <div class="row">
          <div class="row">
            <div class="col-md-9">
              <h3 class="at-fontweight mt-10">
                Deals and Offers{" "}
                <small>
                  ALL OFFERS <span class="fa fa-angle-down" />
                </small>{" "}
              </h3>
            </div>
            <div class="col-md-3">
              <div class="controls pull-right hidden-xs">
                <a
                  class="left fa fa-chevron-left"
                  href="#carousel-example-generic"
                  data-slide="prev"
                />
                <a
                  class="right fa fa-chevron-right"
                  href="#carousel-example-generic"
                  data-slide="next"
                />
              </div>
            </div>
          </div>
          <div
            id="carousel-example-generic"
            class="carousel slide hidden-xs"
            data-ride="carousel"
          >
            <div class="carousel-inner">
              <div class="item active">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="col-item">
                      <div class="photo">
                        <Image
                          src={offer1}
                          class={"img-responsive"}
                          alt={"Company Name"}
                        />
                      </div>
                      <div class="info">
                        <div class="at-dealbr">
                          <h4>
                            <i class="fa fa-inr" />800 off on Indigo Flights
                          </h4>
                          <p>
                            Indigo Offers On Air Tickets: Grab Rs 800 OFF On
                            Domestic Flights
                          </p>
                          <p class="ptb-10 at-coupon">
                            COUPON CODE <span>INDIGO800</span>{" "}
                          </p>
                          <p class="ptb-10 at-coupandis">
                            View Coupon Description
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="col-item">
                      <div class="photo">
                        <Image
                          src={offer2}
                          class={"img-responsive"}
                          alt={"Company Name"}
                        />
                      </div>
                      <div class="info">
                        <div class="at-dealbr">
                          <h4>
                            <i class="fa fa-inr" />800 off on Indigo Flights
                          </h4>
                          <p>
                            Indigo Offers On Air Tickets: Grab Rs 800 OFF On
                            Domestic Flights
                          </p>
                          <p class="ptb-10 at-coupon">
                            COUPON CODE <span>INDIGO800</span>{" "}
                          </p>
                          <p class="ptb-10 at-coupandis">
                            View Coupon Description
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="col-item">
                      <div class="photo">
                        <Image
                          src={offer1}
                          class={"img-responsive"}
                          alt={"Company Name"}
                        />
                      </div>
                      <div class="info">
                        <div class="at-dealbr">
                          <h4>
                            <i class="fa fa-inr" />800 off on Indigo Flights
                          </h4>
                          <p>
                            Indigo Offers On Air Tickets: Grab Rs 800 OFF On
                            Domestic Flights
                          </p>
                          <p class="ptb-10 at-coupon">
                            COUPON CODE <span>INDIGO800</span>{" "}
                          </p>
                          <p class="ptb-10 at-coupandis">
                            View Coupon Description
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="col-item">
                      <div class="photo">
                        <Image
                          src={offer2}
                          class={"img-responsive"}
                          alt={"Company Name"}
                        />
                      </div>
                      <div class="info">
                        <div class="at-dealbr">
                          <h4>
                            <i class="fa fa-inr" />800 off on Indigo Flights
                          </h4>
                          <p>
                            Indigo Offers On Air Tickets: Grab Rs 800 OFF On
                            Domestic Flights
                          </p>
                          <p class="ptb-10 at-coupon">
                            COUPON CODE <span>INDIGO800</span>{" "}
                          </p>
                          <p class="ptb-10 at-coupandis">
                            View Coupon Description
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="col-item">
                      <div class="photo">
                        <img src="img/offer2.jpg" class="img-responsive" />
                      </div>
                      <div class="info">
                        <div class="at-dealbr">
                          <h4>
                            <i class="fa fa-inr" />800 off on Indigo Flights
                          </h4>
                          <p>
                            Indigo Offers On Air Tickets: Grab Rs 800 OFF On
                            Domestic Flights
                          </p>
                          <p class="ptb-10 at-coupon">
                            COUPON CODE <span>INDIGO800</span>{" "}
                          </p>
                          <p class="ptb-10 at-coupandis">
                            View Coupon Description
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="col-item">
                      <div class="photo">
                        <img src="img/indigo.jpg" class="img-responsive" />
                      </div>
                      <div class="info">
                        <div class="at-dealbr">
                          <h4>
                            <i class="fa fa-inr" />800 off on Indigo Flights
                          </h4>
                          <p>
                            Indigo Offers On Air Tickets: Grab Rs 800 OFF On
                            Domestic Flights
                          </p>
                          <p class="ptb-10 at-coupon">
                            COUPON CODE <span>INDIGO800</span>{" "}
                          </p>
                          <p class="ptb-10 at-coupandis">
                            View Coupon Description
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Offers;
