import axios from "axios";
import { connect } from "react-redux";

const connector = requestData => {
  return axios
    .post("http://services.technogramsolutions.com/ums/v1/sign-in", requestData)
    .then(responseData => {
      console.log(responseData);
      return responseData.data;
    });
};

export default connector;
