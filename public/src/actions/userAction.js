import * as R from "ramda";
import getRequestPayload from "./../prehandler/User/Login/getRequestPayload";
import connector from "./../connect/User/Login/connect";
import getDispatchData from "./../dekorators/User/Login/getDispatchData";
const then = R.curry((f, p) => p.then(f));

const loginUser = (userInfo = {}) => {
  return async dispatch => {
    let response = await R.pipe(
      getRequestPayload,
      connector,
      then(getDispatchData)
    )(userInfo);

    dispatch({
      type: response.type,
      payload: response.payload
    });
  };
};
export { loginUser };
