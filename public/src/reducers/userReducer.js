const defaultState = {
  fetching: false,
  error: null,
  success: null,
  userInfo: {}
};
export default function userReducer(state = defaultState, action) {
  switch (action.type) {
    case "USER_LOGIN_SUCCESS": {
      console.log("Action Payload", action.payload);
      const { user } = action.payload;
      return {
        ...state,
        userInfo: user,
        success: true,
        error: false
      };
    }
    case "USER_LOGIN_FAILURE": {
      console.log("Reducer", action.payload);
      let error = action.payload.errors.message;
      return {
        ...state,
        error: error,
        success: false
      };
    }
    case "MANAGE_USER": {
      console.log("Inside Manage User");
      return {
        ...state,
        isloggedIn: false
      };
    }
  }

  return state;
}
