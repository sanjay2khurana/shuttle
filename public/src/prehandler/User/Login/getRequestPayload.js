const getRequestPayload = userInfo => {
  console.log("hello inside get request");
  const { username, password, role } = userInfo;
  return {
    username,
    password,
    role
  };
};

export default getRequestPayload;
