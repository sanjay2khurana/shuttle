import React, { Component } from "react";
import { connect } from "react-redux";

class LoginLayout extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="wrapper">
        <div className="container-fluid">
          <div className="row">
            <div className="login_form">{this.props.children}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginLayout;
