import React, { Component } from "react";
import { connect } from "react-redux";
import Topbar from "./../component/common/Topbar";
import Menubar from "./../component/common/Menubar";
import Footer from "./../component/common/Footer";
class MasterLayout extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount = () => {
    let user = localStorage.getItem("user");
    if (user) {
      const { userInfo } = JSON.parse(user);
      const { name } = userInfo;
      this.setState({
        username: name
      });
    }
  };

  render() {
    return (
      <div>
        <Menubar />
        {this.props.children}
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userInfo: state.users
});
export default connect(mapStateToProps)(MasterLayout);
