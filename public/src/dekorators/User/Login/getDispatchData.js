const getDispatchData = (response = {}) => {
  let type = "USER_LOGIN_SUCCESS";
  if (!response.status.success) {
    type = "USER_LOGIN_FAILURE";
  }

  return {
    type: type,
    payload: response
  };
};

export default getDispatchData;
