import React from "react";
import { Component } from "react";
import MasterLayout from "./../../containers/masterLayout";
import SearchBox from "./../../component/common/SearchBox";
import RecentSearch from "./../../component/common/RecentSearch";
import Offers from "./../../component/common/Offers";
import Notification from "./../../component/common/Notification";

class SearchPage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <MasterLayout>
        <div class="imageRelate pt-80 pb-50">
          <div class="at-searchbox">
            <SearchBox />
          </div>
        </div>
        <RecentSearch />
        <div class="container">
          <div class="row">
            <div class="col-sm-9">
              <Offers />
            </div>
            <div class="col-sm-3">
              <Notification />
            </div>
          </div>
        </div>
      </MasterLayout>
    );
  }
}

export default SearchPage;
