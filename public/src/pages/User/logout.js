import React from "react";
import { Component } from "react";
import Cookie from "react-cookies";

class LogoutPage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
    localStorage.clear("user");
    Cookie.remove("uuid");
    window.location.href = "/";
  };
  render() {
    return <div>You have been logged out successfully.</div>;
  }
}

export default LogoutPage;
