import React from "react";
import { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LoginCss from "./../../../css/login.css";
import FormCss from "./../../../css/form.css";
import LoginLayout from "../../containers/loginLayout";
import { loginUser } from "./../../actions/userAction";
import Textbox from "../../component/common/textbox";
import SelectBox from "../../component/common/selectbox";

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailOrMobile: "",
      password: "",
      role: "",
      error: false,
      success: false,
      roleValues: ["AGENT", "SUPPLIER", "DISTRIBUTOR", "ACCOUNTS", "ADMIN"]
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event, field) {
    this.setState({
      [field]: event.target.value
    });
  }

  doLogin = () => {
    const { emailOrMobile: username, password, role } = this.state;
    console.log(username, password, role);
    if (username && password && role) {
      let userInfo = { username, password, role };
      this.props.dispatch(loginUser({ username, password, role }));
    } else {
      this.setState({ error: true });
    }
  };

  componentWillReceiveProps = nextProps => {
    const { success, userInfo } = nextProps;
    if (success) {
      localStorage.user = JSON.stringify({
        isloggedIn: true,
        userInfo: userInfo
      });
      window.location.href = "/";
    }
  };

  render() {
    const error = this.props.error || this.state.error || null;
    const success = this.props.success || this.state.success || null;
    return (
      <LoginLayout>
        <div class="content_form">
          <div class="col-md-4 col-md-offset-4">
            <h1 class="login_tour">Login to your account</h1>
            <div class="inner_content">
              <div class="row">
                <div class="12">
                  <div class="form_content">
                    {error && (
                      <div className="alert alert-danger warning_msg">
                        <strong>Warning!</strong> {error}
                      </div>
                    )}
                    <Textbox
                      for="email"
                      type="email"
                      text="Email Address"
                      field="emailOrMobile"
                      handleChange={this.handleChange}
                    />
                    <Textbox
                      for="pwd"
                      type="password"
                      text="Password"
                      field="password"
                      handleChange={this.handleChange}
                    />
                    <SelectBox
                      options={this.state.roleValues}
                      field="role"
                      handleChange={this.handleChange}
                    />
                    <div class="row">
                      <div class="col-sm-12 text-right">
                        <a href="#" class="forgetpass">
                          Reset my password ?
                        </a>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="text-center">
                        <button
                          type="submit"
                          onClick={this.doLogin}
                          class="btn btn-success sign_btn"
                        >
                          Submit
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </LoginLayout>
    );
  }
}

const mapStateToProps = state => ({
  error: state.users.error,
  success: state.users.success,
  userInfo: state.users.userInfo
});

const mapDispatchToProps = dispatch => {
  let actions = bindActionCreators({
    loginUser: loginUser
  });
  return { actions, dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
