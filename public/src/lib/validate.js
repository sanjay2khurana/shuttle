export const EmailValidation = email => {
  let re = /^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/;
  return re.test(String(email).toLowerCase());
};

export const MobileValidation = mobile => {
  return mobile.length === 10 ? true : false;
};

const Validation = { EmailValidation, MobileValidation };

export default Validation;
